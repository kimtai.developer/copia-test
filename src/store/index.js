import Vuex from "vuex"
import Vue from "vue"
import Countries from "./modules/countries/index"


Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        Countries
    }
})