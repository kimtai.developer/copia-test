import * as TYPES from "@/store/types.js"

export const mutations = { 
    [TYPES.GET_COUNTRIES] (state,payload) { 
        let all = []
        let options = []

        payload.forEach( el => {

            let country = {};
            country.name = el.name
            country.alpha3Code = el.alpha3Code
            country.flag = el.flag
            country.region = el.region
            country.capital = el.capital
            country.callingCodes = el.callingCodes
            all.push(country)

            let option = {}
            option.code = el.alpha3Code.toLowerCase()
            option.name = el.name

            options.push(option)

        });

        state.countries = all
        state.options = options
    },
    [TYPES.SET_COUNTRY_DETAILS] (state,payload) {
        state.details = payload
    }
}