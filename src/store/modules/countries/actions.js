import API from "@/services/api.js"
import * as TYPES  from "@/store/types.js"

export const actions = { 
    //Action to get all countries
    async fetchCountries( {commit} ) {
        const all = await API.getALLCountries()
        commit (TYPES.GET_COUNTRIES,all)
    },

    //Action to get country details
    async fetchDetails( {commit},payload ) {
        const details = await API.getCountryDetails(payload)
        commit (TYPES.SET_COUNTRY_DETAILS,details)
    },
}