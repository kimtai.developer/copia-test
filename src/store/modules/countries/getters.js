export const getters = {
    countries: state =>  state.countries,
    details: state => state.details,
    options: state => state.options
}