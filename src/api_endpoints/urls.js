// base url
let BASE_URL = "https://restcountries.eu/rest/v2/"

export const ALL_COUNTRIES = BASE_URL + "all"
export const COUNTRY_BY_CODE = BASE_URL + "alpha/"