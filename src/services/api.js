import axios from "axios"
import * as APIENDPOINTS from "@/api_endpoints/urls.js"

export default class ApiService {
    //get all countries
    static async getALLCountries () {
        const all  = await axios.get(APIENDPOINTS.ALL_COUNTRIES)
        return all.data
    }

    //get country details
    static async getCountryDetails (payload) {
        const all  = await axios.get(APIENDPOINTS.COUNTRY_BY_CODE  + payload)
        return all.data
    }
}