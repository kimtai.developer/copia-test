import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "bootstrap";
import 'bootstrap/dist/css/bootstrap.css'
import './assets/css/style.css'
import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';

import Multiselect from 'vue-multiselect'
// register globally
Vue.component('multiselect', Multiselect)
Vue.component('vuephonenumberinput', VuePhoneNumberInput);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
